
# Trigger

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadata** | [**List&lt;Metadata&gt;**](Metadata.md) |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**targetAsset** | [**ReferencedId**](ReferencedId.md) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
ASSETEVENT | &quot;AssetEvent&quot;



