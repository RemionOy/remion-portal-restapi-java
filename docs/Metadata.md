
# Metadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**value** | **String** |  |  [optional]
**datatype** | [**DatatypeEnum**](#DatatypeEnum) |  |  [optional]
**datatypeOptions** | **String** |  |  [optional]
**isReadonly** | **Boolean** |  |  [optional]


<a name="DatatypeEnum"></a>
## Enum: DatatypeEnum
Name | Value
---- | -----
STRING | &quot;String&quot;
TEXT | &quot;Text&quot;
BOOLEAN | &quot;Boolean&quot;
NUMBER | &quot;Number&quot;
MULTISELECT | &quot;Multiselect&quot;
ASSETID | &quot;AssetId&quot;
COLOR | &quot;Color&quot;
PASSWORD | &quot;Password&quot;
URL | &quot;Url&quot;
EMAIL | &quot;Email&quot;
TELNUM | &quot;Telnum&quot;
UNIXTIMEMS | &quot;UnixtimeMs&quot;
TIMEZONE | &quot;Timezone&quot;



