# DefaultApi

All URIs are relative to *https://cloud1.remion.com/regattaportal*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addData**](DefaultApi.md#addData) | **POST** /v1/data/{assetId} | Add new signal data. Note that all values for specific timestamp must be in the same row.
[**changeStatus**](DefaultApi.md#changeStatus) | **PUT** /v1/jobs/{jobId} | Start or stop job
[**createAction**](DefaultApi.md#createAction) | **POST** /v1/trigger_actions | Create TriggerAction
[**createAssetFromTemplate**](DefaultApi.md#createAssetFromTemplate) | **POST** /v1/assets | Create asset from template
[**createAssetTemplate**](DefaultApi.md#createAssetTemplate) | **POST** /v1/assets/template | Create asset template based on which new assets can be created
[**createDashboard**](DefaultApi.md#createDashboard) | **POST** /v1/dashboards | Create dashboard
[**createFile**](DefaultApi.md#createFile) | **POST** /v1/files | Create file
[**createImage**](DefaultApi.md#createImage) | **POST** /v1/images | Create image
[**createJob**](DefaultApi.md#createJob) | **POST** /v1/jobs | Create job
[**createPermission**](DefaultApi.md#createPermission) | **POST** /v1/permissions | Create permission
[**createScript**](DefaultApi.md#createScript) | **POST** /v1/scripts | Create script
[**createUser**](DefaultApi.md#createUser) | **POST** /v1/registration | Register new user
[**createUserFromTemplate**](DefaultApi.md#createUserFromTemplate) | **POST** /v1/users | Create user from template
[**createUserTemplate**](DefaultApi.md#createUserTemplate) | **POST** /v1/users/template | Create user template
[**deleteAction**](DefaultApi.md#deleteAction) | **DELETE** /v1/trigger_actions/{triggerActionId} | Delete TriggerAction
[**deleteAsset**](DefaultApi.md#deleteAsset) | **DELETE** /v1/assets/{assetId} | Delete asset. Will also delete all files and events of this asset.
[**deleteDashboard**](DefaultApi.md#deleteDashboard) | **DELETE** /v1/dashboards/{dashboardId} | Delete dashboard
[**deleteData**](DefaultApi.md#deleteData) | **DELETE** /v1/assets/{assetId}/history | Remove asset&#39;s signal data, events, files, and/or location history
[**deleteFile**](DefaultApi.md#deleteFile) | **DELETE** /v1/files/{fileId} | Delete File
[**deleteImage**](DefaultApi.md#deleteImage) | **DELETE** /v1/images/{imageId} | Delete image
[**deleteJob**](DefaultApi.md#deleteJob) | **DELETE** /v1/jobs/{jobId} | Delete job. If job is running, will try to stop it before deleting it.
[**deletePermission**](DefaultApi.md#deletePermission) | **DELETE** /v1/permissions/{permissionId} | Delete permission. Will remove permission from users and access control of objects.
[**deleteScript**](DefaultApi.md#deleteScript) | **DELETE** /v1/scripts/{scriptId} | Delete script
[**deleteUser**](DefaultApi.md#deleteUser) | **DELETE** /v1/users/{userId} | Delete user
[**exportData**](DefaultApi.md#exportData) | **GET** /v1/data/{assetId}/export | Export signal data to csv file
[**exportEvents**](DefaultApi.md#exportEvents) | **GET** /v1/events/export | Export list of asset events. Produces CSV file.
[**getAction**](DefaultApi.md#getAction) | **GET** /v1/trigger_actions/{triggerActionId} | Get specific TriggerAction
[**getActionCount**](DefaultApi.md#getActionCount) | **GET** /v1/trigger_actions/count | Get TriggerAction count
[**getActions**](DefaultApi.md#getActions) | **GET** /v1/trigger_actions | Get list of TriggerActions user has permissions to see
[**getAsset**](DefaultApi.md#getAsset) | **GET** /v1/assets/{assetId} | Get specific asset
[**getAssetHierarchy**](DefaultApi.md#getAssetHierarchy) | **GET** /v1/assets/hierarchy | Get asset hierarchy
[**getAssets**](DefaultApi.md#getAssets) | **GET** /v1/assets | Get list of assets user has permissions to see
[**getDashboard**](DefaultApi.md#getDashboard) | **GET** /v1/dashboards/{dashboardId} | Get specific dashboard
[**getDashboardCount**](DefaultApi.md#getDashboardCount) | **GET** /v1/dashboards/count | Get dashboard count
[**getDashboards**](DefaultApi.md#getDashboards) | **GET** /v1/dashboards | Get list of dashboards
[**getData**](DefaultApi.md#getData) | **GET** /v1/data/{assetId} | Get existing signal data
[**getEventCount**](DefaultApi.md#getEventCount) | **GET** /v1/events/count | Get asset event count
[**getEvents**](DefaultApi.md#getEvents) | **GET** /v1/events | Get list of asset events
[**getFile**](DefaultApi.md#getFile) | **GET** /v1/files/{fileId} | Get specific file
[**getFileCount**](DefaultApi.md#getFileCount) | **GET** /v1/files/count | Get file count
[**getFiles**](DefaultApi.md#getFiles) | **GET** /v1/files | Get list of files user has permissions to see
[**getImage**](DefaultApi.md#getImage) | **GET** /v1/images/{imageId} | Get specific image
[**getImageCount**](DefaultApi.md#getImageCount) | **GET** /v1/images/count | Get image count
[**getImages**](DefaultApi.md#getImages) | **GET** /v1/images | Get list of images  or find an image by name
[**getJob**](DefaultApi.md#getJob) | **GET** /v1/jobs/{jobId} | Get specific job
[**getJobCount**](DefaultApi.md#getJobCount) | **GET** /v1/jobs/count | Get job count
[**getJobs**](DefaultApi.md#getJobs) | **GET** /v1/jobs | Get list of jobs user has permissions to see
[**getLocationCount**](DefaultApi.md#getLocationCount) | **GET** /v1/locations/count | Get location count
[**getLocations**](DefaultApi.md#getLocations) | **GET** /v1/locations | Get list of locations
[**getPermission**](DefaultApi.md#getPermission) | **GET** /v1/permissions/{permissionId} | Get specific permission
[**getPermissions**](DefaultApi.md#getPermissions) | **GET** /v1/permissions | Get list of permissions user has permissions to see
[**getScript**](DefaultApi.md#getScript) | **GET** /v1/scripts/{scriptId} | Get specific script
[**getScriptCount**](DefaultApi.md#getScriptCount) | **GET** /v1/scripts/count | Get script count
[**getScripts**](DefaultApi.md#getScripts) | **GET** /v1/scripts | Get list of script user has permission to see
[**getServerInfo**](DefaultApi.md#getServerInfo) | **GET** /v1/serverinfo | Get basic information of the server, e.g. version
[**getStatus**](DefaultApi.md#getStatus) | **GET** /v1/status | Get user specific status
[**getUISettings**](DefaultApi.md#getUISettings) | **GET** /v1/ui_settings | Get basic UI settings
[**getUser**](DefaultApi.md#getUser) | **GET** /v1/users/{userId} | Get user
[**getUserCount**](DefaultApi.md#getUserCount) | **GET** /v1/users/count | Get user count
[**getUsers**](DefaultApi.md#getUsers) | **GET** /v1/users | Get list of users user has permissions to see
[**passwordResetRequest**](DefaultApi.md#passwordResetRequest) | **POST** /v1/pwreset | Ask for password reset
[**refreshToken**](DefaultApi.md#refreshToken) | **POST** /v1/session | Get session token
[**resetPassword**](DefaultApi.md#resetPassword) | **PUT** /v1/pwreset | Reset password
[**updateAction**](DefaultApi.md#updateAction) | **PUT** /v1/trigger_actions/{triggerActionId} | Update action
[**updateAsset**](DefaultApi.md#updateAsset) | **PUT** /v1/assets/{assetId} | Update asset
[**updateAssetTemplate**](DefaultApi.md#updateAssetTemplate) | **POST** /v1/assets/{assetId}/template | Update asset template: migrates asset to different template version
[**updateDashboard**](DefaultApi.md#updateDashboard) | **PUT** /v1/dashboards/{dashboardId} | Update dashboard
[**updateFile**](DefaultApi.md#updateFile) | **PUT** /v1/files/{fileId} | Update file
[**updateImage**](DefaultApi.md#updateImage) | **PUT** /v1/images/{imageId} | Update image
[**updatePermission**](DefaultApi.md#updatePermission) | **PUT** /v1/permissions/{permissionId} | Update permission
[**updateScript**](DefaultApi.md#updateScript) | **PUT** /v1/scripts/{scriptId} | Update image
[**updateUser**](DefaultApi.md#updateUser) | **PUT** /v1/users/{userId} | Update user
[**updateUserTemplate**](DefaultApi.md#updateUserTemplate) | **POST** /v1/users/{userId}/template | Update user template: migrates user to different template version


<a name="addData"></a>
# **addData**
> addData(assetId, apikey, body, authorization)

Add new signal data. Note that all values for specific timestamp must be in the same row.



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | ID of asset to add samples
String apikey = "apikey_example"; // String | Asset's apikey. Deprecated and will be removed in the future.
DataCollection body = new DataCollection(); // DataCollection | Data values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\". Asset apikey can be used instead for now, but it will be removed in the future.
try {
    apiInstance.addData(assetId, apikey, body, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#addData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset to add samples |
 **apikey** | **String**| Asset&#39;s apikey. Deprecated and will be removed in the future. | [optional]
 **body** | [**DataCollection**](DataCollection.md)| Data values | [optional]
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot;. Asset apikey can be used instead for now, but it will be removed in the future. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="changeStatus"></a>
# **changeStatus**
> changeStatus(jobId, authorization, command)

Start or stop job



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String jobId = "jobId_example"; // String | Database id of job
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String command = "command_example"; // String | 
try {
    apiInstance.changeStatus(jobId, authorization, command);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#changeStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **String**| Database id of job |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **command** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="createAction"></a>
# **createAction**
> ResourceCreatedResponse createAction(body, authorization)

Create TriggerAction



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
TriggerAction body = new TriggerAction(); // TriggerAction | TriggerAction to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createAction(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createAction");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**TriggerAction**](TriggerAction.md)| TriggerAction to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createAssetFromTemplate"></a>
# **createAssetFromTemplate**
> ResourceCreatedResponse createAssetFromTemplate(body, authorization)

Create asset from template



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
AssetFromTemplate body = new AssetFromTemplate(); // AssetFromTemplate | Basic information and template from which asset is created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createAssetFromTemplate(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createAssetFromTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AssetFromTemplate**](AssetFromTemplate.md)| Basic information and template from which asset is created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createAssetTemplate"></a>
# **createAssetTemplate**
> ResourceCreatedResponse createAssetTemplate(body, authorization)

Create asset template based on which new assets can be created



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Asset body = new Asset(); // Asset | Asset template to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createAssetTemplate(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createAssetTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Asset**](Asset.md)| Asset template to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createDashboard"></a>
# **createDashboard**
> ResourceCreatedResponse createDashboard(body, authorization)

Create dashboard



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Dashboard body = new Dashboard(); // Dashboard | Dashboard to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createDashboard(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createDashboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Dashboard**](Dashboard.md)| Dashboard to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createFile"></a>
# **createFile**
> ResourceCreatedResponse createFile(body, authorization)

Create file



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
AssetFile body = new AssetFile(); // AssetFile | File to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createFile(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**AssetFile**](AssetFile.md)| File to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createImage"></a>
# **createImage**
> ResourceCreatedResponse createImage(body, authorization)

Create image



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Image body = new Image(); // Image | Image to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createImage(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createImage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Image**](Image.md)| Image to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createJob"></a>
# **createJob**
> ResourceCreatedResponse createJob(body, authorization)

Create job



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Job body = new Job(); // Job | Job to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createJob(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createJob");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Job**](Job.md)| Job to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createPermission"></a>
# **createPermission**
> ResourceCreatedResponse createPermission(body, authorization)

Create permission



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Permission body = new Permission(); // Permission | Permission to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createPermission(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createPermission");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Permission**](Permission.md)| Permission to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createScript"></a>
# **createScript**
> ResourceCreatedResponse createScript(body, authorization)

Create script



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Script body = new Script(); // Script | Script to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createScript(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createScript");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Script**](Script.md)| Script to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createUser"></a>
# **createUser**
> BaseRegisteringUser createUser(body)

Register new user



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
BaseRegisteringUser body = new BaseRegisteringUser(); // BaseRegisteringUser | Registration information
try {
    BaseRegisteringUser result = apiInstance.createUser(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**BaseRegisteringUser**](BaseRegisteringUser.md)| Registration information |

### Return type

[**BaseRegisteringUser**](BaseRegisteringUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createUserFromTemplate"></a>
# **createUserFromTemplate**
> ResourceCreatedResponse createUserFromTemplate(body, authorization)

Create user from template



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
UserFromTemplate body = new UserFromTemplate(); // UserFromTemplate | Basic information and template from which user is created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createUserFromTemplate(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createUserFromTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**UserFromTemplate**](UserFromTemplate.md)| Basic information and template from which user is created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="createUserTemplate"></a>
# **createUserTemplate**
> ResourceCreatedResponse createUserTemplate(body, authorization)

Create user template



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
User body = new User(); // User | User template to be created
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    ResourceCreatedResponse result = apiInstance.createUserTemplate(body, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#createUserTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**User**](User.md)| User template to be created |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**ResourceCreatedResponse**](ResourceCreatedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteAction"></a>
# **deleteAction**
> deleteAction(triggerActionId, authorization)

Delete TriggerAction



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String triggerActionId = "triggerActionId_example"; // String | Database id of action
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteAction(triggerActionId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAction");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **triggerActionId** | **String**| Database id of action |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteAsset"></a>
# **deleteAsset**
> deleteAsset(assetId, authorization)

Delete asset. Will also delete all files and events of this asset.



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteAsset(assetId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteDashboard"></a>
# **deleteDashboard**
> deleteDashboard(dashboardId, authorization)

Delete dashboard



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String dashboardId = "dashboardId_example"; // String | Database id of dashboard
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteDashboard(dashboardId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteDashboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboardId** | **String**| Database id of dashboard |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteData"></a>
# **deleteData**
> deleteData(assetId, body, authorization)

Remove asset&#39;s signal data, events, files, and/or location history



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
AssetHistoryRemovalParams body = new AssetHistoryRemovalParams(); // AssetHistoryRemovalParams | What items should be removed
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteData(assetId, body, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **body** | [**AssetHistoryRemovalParams**](AssetHistoryRemovalParams.md)| What items should be removed |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteFile"></a>
# **deleteFile**
> deleteFile(fileId, authorization)

Delete File



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String fileId = "fileId_example"; // String | Database id of file
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteFile(fileId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileId** | **String**| Database id of file |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteImage"></a>
# **deleteImage**
> deleteImage(imageId, authorization)

Delete image



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String imageId = "imageId_example"; // String | Database id of image
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteImage(imageId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteImage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **imageId** | **String**| Database id of image |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteJob"></a>
# **deleteJob**
> deleteJob(jobId, authorization)

Delete job. If job is running, will try to stop it before deleting it.



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String jobId = "jobId_example"; // String | Database id of job
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteJob(jobId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteJob");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **String**| Database id of job |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deletePermission"></a>
# **deletePermission**
> deletePermission(permissionId, authorization)

Delete permission. Will remove permission from users and access control of objects.



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String permissionId = "permissionId_example"; // String | Database id of permission
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deletePermission(permissionId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deletePermission");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **permissionId** | **String**| Database id of permission |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteScript"></a>
# **deleteScript**
> deleteScript(scriptId, authorization)

Delete script



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String scriptId = "scriptId_example"; // String | Database id of script
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteScript(scriptId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteScript");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scriptId** | **String**| Database id of script |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="deleteUser"></a>
# **deleteUser**
> deleteUser(userId, authorization)

Delete user



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String userId = "userId_example"; // String | Database id of user
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.deleteUser(userId, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#deleteUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| Database id of user |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exportData"></a>
# **exportData**
> exportData(assetId, apikey, from, to, resolution, aggregation, names, authorization)

Export signal data to csv file



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | ID of asset of which samples are required
String apikey = "apikey_example"; // String | Asset's apikey. Deprecated and will be removed in the future.
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
Integer resolution = 56; // Integer | Aggregation method, used with resolution
String aggregation = "aggregation_example"; // String | Used with aggregation: time period will be devided into this many slices and aggregation will be calculated for each
String names = "names_example"; // String | Comma-separated list of signal names to export
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\". Asset apikey can be used instead for now, but it will be removed in the future.
try {
    apiInstance.exportData(assetId, apikey, from, to, resolution, aggregation, names, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#exportData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset of which samples are required |
 **apikey** | **String**| Asset&#39;s apikey. Deprecated and will be removed in the future. |
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. |
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. |
 **resolution** | **Integer**| Aggregation method, used with resolution |
 **aggregation** | **String**| Used with aggregation: time period will be devided into this many slices and aggregation will be calculated for each |
 **names** | **String**| Comma-separated list of signal names to export |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot;. Asset apikey can be used instead for now, but it will be removed in the future. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="exportEvents"></a>
# **exportEvents**
> String exportEvents(assetId, authorization, activeOnly, from, to, textFilter, textFilterFields, levelFilter)

Export list of asset events. Produces CSV file.



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean activeOnly = true; // Boolean | Whether to get only active events. Default value is false.
Long from = 789L; // Long | Only data with timestamps equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamps equal or less than this value are retrieved. Unix time in milliseconds.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
String levelFilter = "levelFilter_example"; // String | Event severity level
try {
    String result = apiInstance.exportEvents(assetId, authorization, activeOnly, from, to, textFilter, textFilterFields, levelFilter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#exportEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **activeOnly** | **Boolean**| Whether to get only active events. Default value is false. | [optional]
 **from** | **Long**| Only data with timestamps equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamps equal or less than this value are retrieved. Unix time in milliseconds. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **levelFilter** | **String**| Event severity level | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAction"></a>
# **getAction**
> TriggerAction getAction(triggerActionId, authorization)

Get specific TriggerAction



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String triggerActionId = "triggerActionId_example"; // String | Database id of action
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    TriggerAction result = apiInstance.getAction(triggerActionId, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAction");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **triggerActionId** | **String**| Database id of action |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**TriggerAction**](TriggerAction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getActionCount"></a>
# **getActionCount**
> Count getActionCount(assetId, authorization, textFilter, textFilterFields)

Get TriggerAction count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Trigger target asset id
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
try {
    Count result = apiInstance.getActionCount(assetId, authorization, textFilter, textFilterFields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getActionCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Trigger target asset id |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getActions"></a>
# **getActions**
> List&lt;TriggerAction&gt; getActions(assetId, authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir)

Get list of TriggerActions user has permissions to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Trigger target asset id
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    List<TriggerAction> result = apiInstance.getActions(assetId, authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getActions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Trigger target asset id |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**List&lt;TriggerAction&gt;**](TriggerAction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAsset"></a>
# **getAsset**
> Asset getAsset(assetId, authorization, fields, download)

Get specific asset



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
Boolean download = true; // Boolean | Whether to force browsers to download asset json
try {
    Asset result = apiInstance.getAsset(assetId, authorization, fields, download);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **download** | **Boolean**| Whether to force browsers to download asset json | [optional]

### Return type

[**Asset**](Asset.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAssetHierarchy"></a>
# **getAssetHierarchy**
> String getAssetHierarchy(hierarchyId, authorization, fields, limit, offset, excludeTemplates, excludeTypes, hierarchyPath)

Get asset hierarchy



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String hierarchyId = "hierarchyId_example"; // String | Database id of hierarchy script
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
Boolean excludeTemplates = false; // Boolean | Exclude templates
String excludeTypes = "excludeTypes_example"; // String | Exclude types. Comma-separated list.
String hierarchyPath = "hierarchyPath_example"; // String | Selected path of hierarchy
try {
    String result = apiInstance.getAssetHierarchy(hierarchyId, authorization, fields, limit, offset, excludeTemplates, excludeTypes, hierarchyPath);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAssetHierarchy");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hierarchyId** | **String**| Database id of hierarchy script |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **excludeTemplates** | **Boolean**| Exclude templates | [optional] [default to false]
 **excludeTypes** | **String**| Exclude types. Comma-separated list. | [optional]
 **hierarchyPath** | **String**| Selected path of hierarchy | [optional]

### Return type

**String**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getAssets"></a>
# **getAssets**
> Assets getAssets(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir, excludeTemplates, excludeTypes, hierarchyId, hierarchyPath, templatesOnly, count)

Get list of assets user has permissions to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
Boolean excludeTemplates = false; // Boolean | Exclude templates
String excludeTypes = "excludeTypes_example"; // String | Exclude types. Comma-separated list.
String hierarchyId = "hierarchyId_example"; // String | Database id of hierarchy script
String hierarchyPath = "hierarchyPath_example"; // String | Selected path of hierarchy
Boolean templatesOnly = false; // Boolean | Templates only
Boolean count = false; // Boolean | Whether to return total count. If false, returns asset array, else returns object. Defaults to false but should be set true as this option may be dropped in the future after which count is always returned.Also note that this is the only way to get total count with hierarchy filtering!
try {
    Assets result = apiInstance.getAssets(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir, excludeTemplates, excludeTypes, hierarchyId, hierarchyPath, templatesOnly, count);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getAssets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]
 **excludeTemplates** | **Boolean**| Exclude templates | [optional] [default to false]
 **excludeTypes** | **String**| Exclude types. Comma-separated list. | [optional]
 **hierarchyId** | **String**| Database id of hierarchy script | [optional]
 **hierarchyPath** | **String**| Selected path of hierarchy | [optional]
 **templatesOnly** | **Boolean**| Templates only | [optional] [default to false]
 **count** | **Boolean**| Whether to return total count. If false, returns asset array, else returns object. Defaults to false but should be set true as this option may be dropped in the future after which count is always returned.Also note that this is the only way to get total count with hierarchy filtering! | [optional] [default to false]

### Return type

[**Assets**](Assets.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDashboard"></a>
# **getDashboard**
> Dashboard getDashboard(dashboardId, authorization, fields)

Get specific dashboard



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String dashboardId = "dashboardId_example"; // String | Database id of dashboard
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
try {
    Dashboard result = apiInstance.getDashboard(dashboardId, authorization, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getDashboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboardId** | **String**| Database id of dashboard |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]

### Return type

[**Dashboard**](Dashboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDashboardCount"></a>
# **getDashboardCount**
> Count getDashboardCount(authorization, textFilter, textFilterFields, assetId, tabsOnly, excludeTabs)

Get dashboard count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
String assetId = "assetId_example"; // String | Id of asset. Only dashboards compatible with asset will be returned.
Boolean tabsOnly = false; // Boolean | Whether to return only dashboards that are tabs
Boolean excludeTabs = false; // Boolean | Whether to return only dashboards that are not tabs
try {
    Count result = apiInstance.getDashboardCount(authorization, textFilter, textFilterFields, assetId, tabsOnly, excludeTabs);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getDashboardCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **assetId** | **String**| Id of asset. Only dashboards compatible with asset will be returned. | [optional]
 **tabsOnly** | **Boolean**| Whether to return only dashboards that are tabs | [optional] [default to false]
 **excludeTabs** | **Boolean**| Whether to return only dashboards that are not tabs | [optional] [default to false]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getDashboards"></a>
# **getDashboards**
> List&lt;Dashboard&gt; getDashboards(authorization, fields, textFilter, textFilterFields, assetId, tabsOnly, excludeTabs, limit, offset, sortBy, sortDir)

Get list of dashboards



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
String assetId = "assetId_example"; // String | Id of asset. Only dashboards compatible with asset will be returned.
Boolean tabsOnly = false; // Boolean | Whether to return only dashboards that are tabs
Boolean excludeTabs = false; // Boolean | Whether to return only dashboards that are not tabs
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    List<Dashboard> result = apiInstance.getDashboards(authorization, fields, textFilter, textFilterFields, assetId, tabsOnly, excludeTabs, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getDashboards");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **assetId** | **String**| Id of asset. Only dashboards compatible with asset will be returned. | [optional]
 **tabsOnly** | **Boolean**| Whether to return only dashboards that are tabs | [optional] [default to false]
 **excludeTabs** | **Boolean**| Whether to return only dashboards that are not tabs | [optional] [default to false]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**List&lt;Dashboard&gt;**](Dashboard.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getData"></a>
# **getData**
> DataCollection getData(assetId, names, aggregation, resolution, apikey, limit, from, to, offset, query, authorization)

Get existing signal data



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | ID of asset of which samples are required
String names = "names_example"; // String | Comma-separated list of signal names
String aggregation = "aggregation_example"; // String | Aggregation methods, comma-separated list
Integer resolution = 56; // Integer | Used with aggregation: time period will be devided into this many slices and aggregation will be calculated for each
String apikey = "apikey_example"; // String | Asset's apikey. Deprecated and will be removed in the future.
Integer limit = 56; // Integer | Maximum number of data items retrieved
Long from = 789L; // Long | Only data with timestamps equal or greater than this value are retrieved. Unix time.
Long to = 789L; // Long | Only data with timestamps equal or less than this value are retrieved. Unix time.
Integer offset = 56; // Integer | How many results are skipped
String query = "query_example"; // String | Deprecated and will be removed in the future.
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\". Asset apikey can be used instead for now, but it will be removed in the future.
try {
    DataCollection result = apiInstance.getData(assetId, names, aggregation, resolution, apikey, limit, from, to, offset, query, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset of which samples are required |
 **names** | **String**| Comma-separated list of signal names |
 **aggregation** | **String**| Aggregation methods, comma-separated list | [enum: mean, min, max, count, median, sum, first, last, difference, stddev]
 **resolution** | **Integer**| Used with aggregation: time period will be devided into this many slices and aggregation will be calculated for each |
 **apikey** | **String**| Asset&#39;s apikey. Deprecated and will be removed in the future. | [optional]
 **limit** | **Integer**| Maximum number of data items retrieved | [optional]
 **from** | **Long**| Only data with timestamps equal or greater than this value are retrieved. Unix time. | [optional]
 **to** | **Long**| Only data with timestamps equal or less than this value are retrieved. Unix time. | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **query** | **String**| Deprecated and will be removed in the future. | [optional]
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot;. Asset apikey can be used instead for now, but it will be removed in the future. | [optional]

### Return type

[**DataCollection**](DataCollection.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEventCount"></a>
# **getEventCount**
> Count getEventCount(assetId, authorization, activeOnly, from, to, textFilter, textFilterFields, levelFilter)

Get asset event count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean activeOnly = true; // Boolean | Whether to get only active events. Default value is false.
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
String levelFilter = "levelFilter_example"; // String | Event severity level
try {
    Count result = apiInstance.getEventCount(assetId, authorization, activeOnly, from, to, textFilter, textFilterFields, levelFilter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getEventCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **activeOnly** | **Boolean**| Whether to get only active events. Default value is false. | [optional]
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **levelFilter** | **String**| Event severity level | [optional]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getEvents"></a>
# **getEvents**
> AssetEvent getEvents(assetId, authorization, activeOnly, from, to, textFilter, textFilterFields, levelFilter, limit, offset, sortBy, sortDir)

Get list of asset events



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean activeOnly = true; // Boolean | Whether to get only active events. Default value is false.
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
String levelFilter = "levelFilter_example"; // String | Event severity level
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    AssetEvent result = apiInstance.getEvents(assetId, authorization, activeOnly, from, to, textFilter, textFilterFields, levelFilter, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **activeOnly** | **Boolean**| Whether to get only active events. Default value is false. | [optional]
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **levelFilter** | **String**| Event severity level | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**AssetEvent**](AssetEvent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFile"></a>
# **getFile**
> AssetFile getFile(fileId, authorization, loadContent, download)

Get specific file



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String fileId = "fileId_example"; // String | Database id of file
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean loadContent = true; // Boolean | File content will not be retrieved if false. Default is true.
Boolean download = true; // Boolean | Will force browsers to download file json if true. Default is false.
try {
    AssetFile result = apiInstance.getFile(fileId, authorization, loadContent, download);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileId** | **String**| Database id of file |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **loadContent** | **Boolean**| File content will not be retrieved if false. Default is true. | [optional]
 **download** | **Boolean**| Will force browsers to download file json if true. Default is false. | [optional]

### Return type

[**AssetFile**](AssetFile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json,application/octet-stream

<a name="getFileCount"></a>
# **getFileCount**
> Count getFileCount(assetId, authorization, from, to, textFilter, textFilterFields)

Get file count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Files' parent asset id
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
try {
    Count result = apiInstance.getFileCount(assetId, authorization, from, to, textFilter, textFilterFields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getFileCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Files&#39; parent asset id |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getFiles"></a>
# **getFiles**
> List&lt;AssetFile&gt; getFiles(assetId, authorization, fields, from, to, textFilter, textFilterFields, limit, offset, sortBy, sortDir)

Get list of files user has permissions to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Files' parent asset id
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    List<AssetFile> result = apiInstance.getFiles(assetId, authorization, fields, from, to, textFilter, textFilterFields, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Files&#39; parent asset id |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**List&lt;AssetFile&gt;**](AssetFile.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getImage"></a>
# **getImage**
> Image getImage(imageId, authorization, fields)

Get specific image



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String imageId = "imageId_example"; // String | Database id of image
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
try {
    Image result = apiInstance.getImage(imageId, authorization, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getImage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **imageId** | **String**| Database id of image |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]

### Return type

[**Image**](Image.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getImageCount"></a>
# **getImageCount**
> Count getImageCount(authorization, textFilter, textFilterFields)

Get image count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
try {
    Count result = apiInstance.getImageCount(authorization, textFilter, textFilterFields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getImageCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getImages"></a>
# **getImages**
> List&lt;Image&gt; getImages(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir)

Get list of images  or find an image by name



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    List<Image> result = apiInstance.getImages(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getImages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**List&lt;Image&gt;**](Image.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getJob"></a>
# **getJob**
> Job getJob(jobId, authorization, fields)

Get specific job



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String jobId = "jobId_example"; // String | Database id of job
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
try {
    Job result = apiInstance.getJob(jobId, authorization, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getJob");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **String**| Database id of job |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]

### Return type

[**Job**](Job.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getJobCount"></a>
# **getJobCount**
> Count getJobCount(authorization, textFilter, textFilterFields)

Get job count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
try {
    Count result = apiInstance.getJobCount(authorization, textFilter, textFilterFields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getJobCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getJobs"></a>
# **getJobs**
> List&lt;Job&gt; getJobs(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir)

Get list of jobs user has permissions to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    List<Job> result = apiInstance.getJobs(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getJobs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**List&lt;Job&gt;**](Job.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLocationCount"></a>
# **getLocationCount**
> Long getLocationCount(assetId, authorization, from, to)

Get location count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
try {
    Long result = apiInstance.getLocationCount(assetId, authorization, from, to);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getLocationCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. | [optional]

### Return type

**Long**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getLocations"></a>
# **getLocations**
> LocationEntity getLocations(assetId, authorization, from, to, limit, offset, sortBy, sortDir)

Get list of locations



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Long from = 789L; // Long | Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds.
Long to = 789L; // Long | Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds.
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    LocationEntity result = apiInstance.getLocations(assetId, authorization, from, to, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **from** | **Long**| Only data with timestamp equal or greater than this value are retrieved. Unix time in milliseconds. | [optional]
 **to** | **Long**| Only data with timestamp equal or less than this value are retrieved. Unix time in milliseconds. | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**LocationEntity**](LocationEntity.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPermission"></a>
# **getPermission**
> Permission getPermission(permissionId, authorization)

Get specific permission



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String permissionId = "permissionId_example"; // String | Database id of permission
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    Permission result = apiInstance.getPermission(permissionId, authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getPermission");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **permissionId** | **String**| Database id of permission |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**Permission**](Permission.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getPermissions"></a>
# **getPermissions**
> List&lt;Permission&gt; getPermissions(authorization, type, limit, offset)

Get list of permissions user has permissions to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String type = "type_example"; // String | 
Integer limit = 56; // Integer | Maximum number of permissions retrieved
Integer offset = 56; // Integer | How many results are skipped
try {
    List<Permission> result = apiInstance.getPermissions(authorization, type, limit, offset);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getPermissions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **type** | **String**|  | [optional]
 **limit** | **Integer**| Maximum number of permissions retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]

### Return type

[**List&lt;Permission&gt;**](Permission.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getScript"></a>
# **getScript**
> Script getScript(scriptId, authorization, fields)

Get specific script



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String scriptId = "scriptId_example"; // String | Database id of script
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
try {
    Script result = apiInstance.getScript(scriptId, authorization, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getScript");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scriptId** | **String**| Database id of script |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]

### Return type

[**Script**](Script.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getScriptCount"></a>
# **getScriptCount**
> Count getScriptCount(authorization, type, textFilter, textFilterFields)

Get script count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String type = "type_example"; // String | Type of scripts, e.g. 'assetfilter' or 'hierarchy'
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
try {
    Count result = apiInstance.getScriptCount(authorization, type, textFilter, textFilterFields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getScriptCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **type** | **String**| Type of scripts, e.g. &#39;assetfilter&#39; or &#39;hierarchy&#39; | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getScripts"></a>
# **getScripts**
> List&lt;Script&gt; getScripts(authorization, fields, type, textFilter, textFilterFields, limit, offset, sortBy, sortDir)

Get list of script user has permission to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String type = "type_example"; // String | Type of scripts, e.g. 'asset filter' or 'hierarchy'
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
try {
    List<Script> result = apiInstance.getScripts(authorization, fields, type, textFilter, textFilterFields, limit, offset, sortBy, sortDir);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getScripts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **type** | **String**| Type of scripts, e.g. &#39;asset filter&#39; or &#39;hierarchy&#39; | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]

### Return type

[**List&lt;Script&gt;**](Script.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getServerInfo"></a>
# **getServerInfo**
> ServerInfo getServerInfo()

Get basic information of the server, e.g. version



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    ServerInfo result = apiInstance.getServerInfo();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getServerInfo");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ServerInfo**](ServerInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getStatus"></a>
# **getStatus**
> List&lt;Status&gt; getStatus(authorization)

Get user specific status



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    List<Status> result = apiInstance.getStatus(authorization);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getStatus");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

[**List&lt;Status&gt;**](Status.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUISettings"></a>
# **getUISettings**
> UISettings getUISettings()

Get basic UI settings



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    UISettings result = apiInstance.getUISettings();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getUISettings");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**UISettings**](UISettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUser"></a>
# **getUser**
> User getUser(userId, authorization, fields)

Get user



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String userId = "userId_example"; // String | Database id of user
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
try {
    User result = apiInstance.getUser(userId, authorization, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| Database id of user |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUserCount"></a>
# **getUserCount**
> Count getUserCount(authorization, textFilter, textFilterFields, excludeTemplates, templatesOnly)

Get user count



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Boolean excludeTemplates = false; // Boolean | Exclude templates
Boolean templatesOnly = false; // Boolean | Templates only
try {
    Count result = apiInstance.getUserCount(authorization, textFilter, textFilterFields, excludeTemplates, templatesOnly);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getUserCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **excludeTemplates** | **Boolean**| Exclude templates | [optional] [default to false]
 **templatesOnly** | **Boolean**| Templates only | [optional] [default to false]

### Return type

[**Count**](Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="getUsers"></a>
# **getUsers**
> List&lt;User&gt; getUsers(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir, excludeTemplates, templatesOnly)

Get list of users user has permissions to see



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
String fields = "fields_example"; // String | Comma-separated list of properties to be fetched. If empty or missing, all fields are returned.
String textFilter = "textFilter_example"; // String | Search string
String textFilterFields = "textFilterFields_example"; // String | Comma-separated list of properties which are searched for textFilter
Integer limit = 56; // Integer | Maximum number of results retrieved
Integer offset = 56; // Integer | How many results are skipped
String sortBy = "sortBy_example"; // String | Name of the field results are sorted by
String sortDir = "sortDir_example"; // String | Asc or desc
Boolean excludeTemplates = false; // Boolean | Exclude templates
Boolean templatesOnly = false; // Boolean | Templates only
try {
    List<User> result = apiInstance.getUsers(authorization, fields, textFilter, textFilterFields, limit, offset, sortBy, sortDir, excludeTemplates, templatesOnly);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#getUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **fields** | **String**| Comma-separated list of properties to be fetched. If empty or missing, all fields are returned. | [optional]
 **textFilter** | **String**| Search string | [optional]
 **textFilterFields** | **String**| Comma-separated list of properties which are searched for textFilter | [optional]
 **limit** | **Integer**| Maximum number of results retrieved | [optional]
 **offset** | **Integer**| How many results are skipped | [optional]
 **sortBy** | **String**| Name of the field results are sorted by | [optional]
 **sortDir** | **String**| Asc or desc | [optional]
 **excludeTemplates** | **Boolean**| Exclude templates | [optional] [default to false]
 **templatesOnly** | **Boolean**| Templates only | [optional] [default to false]

### Return type

[**List&lt;User&gt;**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="passwordResetRequest"></a>
# **passwordResetRequest**
> passwordResetRequest(email)

Ask for password reset



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String email = "email_example"; // String | e-mail address
try {
    apiInstance.passwordResetRequest(email);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#passwordResetRequest");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| e-mail address |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="refreshToken"></a>
# **refreshToken**
> SessionResponse refreshToken(body)

Get session token



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
Credentials body = new Credentials(); // Credentials | Login information
try {
    SessionResponse result = apiInstance.refreshToken(body);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#refreshToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Credentials**](Credentials.md)| Login information |

### Return type

[**SessionResponse**](SessionResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resetPassword"></a>
# **resetPassword**
> resetPassword(mode, body)

Reset password



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String mode = "forgotpw"; // String | Either \"newpw\" or \"forgotpw\"
PasswordResetRequest body = new PasswordResetRequest(); // PasswordResetRequest | 
try {
    apiInstance.resetPassword(mode, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#resetPassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mode** | **String**| Either \&quot;newpw\&quot; or \&quot;forgotpw\&quot; | [default to forgotpw]
 **body** | [**PasswordResetRequest**](PasswordResetRequest.md)|  |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="updateAction"></a>
# **updateAction**
> updateAction(triggerActionId, body, authorization, patch)

Update action



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String triggerActionId = "triggerActionId_example"; // String | Database id of action
TriggerAction body = new TriggerAction(); // TriggerAction | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
try {
    apiInstance.updateAction(triggerActionId, body, authorization, patch);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateAction");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **triggerActionId** | **String**| Database id of action |
 **body** | [**TriggerAction**](TriggerAction.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateAsset"></a>
# **updateAsset**
> updateAsset(assetId, body, authorization, patch, dryrun)

Update asset



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
Asset body = new Asset(); // Asset | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
Boolean dryrun = true; // Boolean | Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true.
try {
    apiInstance.updateAsset(assetId, body, authorization, patch, dryrun);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **body** | [**Asset**](Asset.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]
 **dryrun** | **Boolean**| Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateAssetTemplate"></a>
# **updateAssetTemplate**
> MigrationResult updateAssetTemplate(assetId, templateId, authorization, dryrun)

Update asset template: migrates asset to different template version



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String assetId = "assetId_example"; // String | Database id of asset
String templateId = "templateId_example"; // String | Id of new template
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean dryrun = true; // Boolean | Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true.
try {
    MigrationResult result = apiInstance.updateAssetTemplate(assetId, templateId, authorization, dryrun);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateAssetTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| Database id of asset |
 **templateId** | **String**| Id of new template |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **dryrun** | **Boolean**| Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true. | [optional] [default to true]

### Return type

[**MigrationResult**](MigrationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateDashboard"></a>
# **updateDashboard**
> updateDashboard(dashboardId, body, authorization, patch)

Update dashboard



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String dashboardId = "dashboardId_example"; // String | Database id of dashboard
Dashboard body = new Dashboard(); // Dashboard | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
try {
    apiInstance.updateDashboard(dashboardId, body, authorization, patch);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateDashboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dashboardId** | **String**| Database id of dashboard |
 **body** | [**Dashboard**](Dashboard.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateFile"></a>
# **updateFile**
> updateFile(fileId, body, authorization, patch)

Update file



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String fileId = "fileId_example"; // String | Database id of file
AssetFile body = new AssetFile(); // AssetFile | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
try {
    apiInstance.updateFile(fileId, body, authorization, patch);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileId** | **String**| Database id of file |
 **body** | [**AssetFile**](AssetFile.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateImage"></a>
# **updateImage**
> updateImage(imageId, body, authorization, patch)

Update image



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String imageId = "imageId_example"; // String | Database id of image
Image body = new Image(); // Image | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
try {
    apiInstance.updateImage(imageId, body, authorization, patch);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateImage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **imageId** | **String**| Database id of image |
 **body** | [**Image**](Image.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updatePermission"></a>
# **updatePermission**
> updatePermission(permissionId, body, authorization)

Update permission



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String permissionId = "permissionId_example"; // String | Database id of permission
Permission body = new Permission(); // Permission | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
try {
    apiInstance.updatePermission(permissionId, body, authorization);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updatePermission");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **permissionId** | **String**| Database id of permission |
 **body** | [**Permission**](Permission.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateScript"></a>
# **updateScript**
> updateScript(scriptId, body, authorization, patch)

Update image



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String scriptId = "scriptId_example"; // String | Database id of script
Script body = new Script(); // Script | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
try {
    apiInstance.updateScript(scriptId, body, authorization, patch);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateScript");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **scriptId** | **String**| Database id of script |
 **body** | [**Script**](Script.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateUser"></a>
# **updateUser**
> updateUser(userId, body, authorization, patch, dryrun)

Update user



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String userId = "userId_example"; // String | Database id of user
User body = new User(); // User | Updated values
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean patch = true; // Boolean | If true, only non-null values are updated. Default value is true.
Boolean dryrun = true; // Boolean | Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true.
try {
    apiInstance.updateUser(userId, body, authorization, patch, dryrun);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| Database id of user |
 **body** | [**User**](User.md)| Updated values |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **patch** | **Boolean**| If true, only non-null values are updated. Default value is true. | [optional]
 **dryrun** | **Boolean**| Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true. | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateUserTemplate"></a>
# **updateUserTemplate**
> MigrationResult updateUserTemplate(userId, templateId, authorization, dryrun)

Update user template: migrates user to different template version



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String userId = "userId_example"; // String | Database id of asset
String templateId = "templateId_example"; // String | Id of new template
String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\"
Boolean dryrun = true; // Boolean | Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true.
try {
    MigrationResult result = apiInstance.updateUserTemplate(userId, templateId, authorization, dryrun);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#updateUserTemplate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **String**| Database id of asset |
 **templateId** | **String**| Id of new template |
 **authorization** | **String**| Authorization token, e.g. \&quot;Bearer (token)\&quot; |
 **dryrun** | **Boolean**| Only matters if asset is a template. If true, will only produce a report of what would be changed, if false, will update template and instances. Default value is true. | [optional] [default to true]

### Return type

[**MigrationResult**](MigrationResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

