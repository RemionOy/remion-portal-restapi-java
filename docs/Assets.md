
# Assets

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assets** | [**List&lt;Asset&gt;**](Asset.md) |  |  [optional]
**totalCount** | **Long** |  |  [optional]



