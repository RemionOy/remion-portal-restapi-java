
# ServerInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**version** | **String** |  |  [optional]
**startTime** | **Long** |  |  [optional]
**libVersions** | **Map&lt;String, String&gt;** |  |  [optional]



