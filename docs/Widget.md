
# Widget

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**type** | [**TypeEnum**](#TypeEnum) |  |  [optional]
**isPublic** | **Boolean** |  |  [optional]
**row** | **Integer** |  |  [optional]
**column** | **Integer** |  |  [optional]
**height** | **Integer** |  |  [optional]
**signalNames** | **List&lt;String&gt;** |  |  [optional]
**pieChartOpts** | [**WidgetPieChartOptions**](WidgetPieChartOptions.md) |  |  [optional]
**lineChartOpts** | [**WidgetLineChartOptions**](WidgetLineChartOptions.md) |  |  [optional]
**barChartOpts** | [**WidgetBarChartOptions**](WidgetBarChartOptions.md) |  |  [optional]
**radialChartOpts** | [**WidgetRadialChartOptions**](WidgetRadialChartOptions.md) |  |  [optional]
**customOpts** | [**WidgetCustomOptions**](WidgetCustomOptions.md) |  |  [optional]
**textOpts** | [**WidgetTextOptions**](WidgetTextOptions.md) |  |  [optional]
**labelOpts** | [**WidgetLabelOptions**](WidgetLabelOptions.md) |  |  [optional]
**imageOpts** | [**WidgetImageOptions**](WidgetImageOptions.md) |  |  [optional]
**cameraOpts** | [**WidgetCameraOptions**](WidgetCameraOptions.md) |  |  [optional]
**signalListOpts** | [**WidgetSignalListOptions**](WidgetSignalListOptions.md) |  |  [optional]
**canvasOpts** | [**WidgetCanvasOptions**](WidgetCanvasOptions.md) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----
LINECHART | &quot;linechart&quot;
PIECHART | &quot;piechart&quot;
BARCHART | &quot;barchart&quot;
RADIAL | &quot;radial&quot;
BIGNUM | &quot;bignum&quot;
TEXT | &quot;text&quot;
IMAGE | &quot;image&quot;
MAP | &quot;map&quot;
CUSTOM | &quot;custom&quot;
LABEL | &quot;label&quot;
SIGNALLIST | &quot;signallist&quot;
ALERTS | &quot;alerts&quot;
CANVAS | &quot;canvas&quot;



