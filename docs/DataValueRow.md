
# DataValueRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Long** | Unix time in ms |  [optional]
**values** | **List&lt;Object&gt;** | Supported values in array are string-number maps and numbers. If there aremultiple rows, each map of given signal must have same keys. |  [optional]



