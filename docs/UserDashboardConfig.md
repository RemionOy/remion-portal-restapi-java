
# UserDashboardConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assetTypes** | [**List&lt;DashboardReference&gt;**](DashboardReference.md) |  |  [optional]



