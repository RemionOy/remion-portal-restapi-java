
# MigrationResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **String** |  |  [optional]
**dryRun** | **Boolean** |  |  [optional]
**success** | **Boolean** |  |  [optional]



