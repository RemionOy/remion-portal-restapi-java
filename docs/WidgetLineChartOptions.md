
# WidgetLineChartOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeRangeSeconds** | **Integer** |  |  [optional]
**dataPoints** | **Integer** |  |  [optional]
**yAxisMin** | **Float** |  |  [optional]
**yAxisMax** | **Float** |  |  [optional]
**aggregationType** | **String** |  |  [optional]
**stepModeEnabled** | **Boolean** |  |  [optional]
**colors** | **List&lt;String&gt;** |  |  [optional]
**showHighAlarmLimit** | **Boolean** |  |  [optional]
**showLowAlarmLimit** | **Boolean** |  |  [optional]
**showHighWarningLimit** | **Boolean** |  |  [optional]
**showLowWarningLimit** | **Boolean** |  |  [optional]



