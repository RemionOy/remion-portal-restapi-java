
# ParameterValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **String** |  |  [optional]
**value** | **String** |  |  [optional]
**timestamp** | **Long** |  |  [optional]



