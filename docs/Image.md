
# Image

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**accessControl** | [**List&lt;AccessControl&gt;**](AccessControl.md) |  |  [optional]
**name** | **String** |  |  [optional]
**contentType** | **String** |  |  [optional]
**base64Data** | **String** |  |  [optional]
**location** | [**LocationEmbedded**](LocationEmbedded.md) |  |  [optional]
**createdDate** | **Long** | Unix time in ms |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]



