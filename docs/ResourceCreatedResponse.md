
# ResourceCreatedResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**location** | **String** |  |  [optional]
**id** | **String** |  |  [optional]



