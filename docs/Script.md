
# Script

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**accessControl** | [**List&lt;AccessControl&gt;**](AccessControl.md) |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**isPublic** | **Boolean** |  |  [optional]
**script** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



