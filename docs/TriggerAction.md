
# TriggerAction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**accessControl** | [**List&lt;AccessControl&gt;**](AccessControl.md) |  |  [optional]
**metadata** | [**List&lt;Metadata&gt;**](Metadata.md) |  |  [optional]
**trigger** | [**Trigger**](Trigger.md) |  |  [optional]
**actions** | [**List&lt;Action&gt;**](Action.md) |  |  [optional]



