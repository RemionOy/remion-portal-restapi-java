
# WidgetCanvasItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**positionX** | **Integer** |  |  [optional]
**positionY** | **Integer** |  |  [optional]
**zOrder** | **Integer** |  |  [optional]
**showCondition** | **String** |  |  [optional]



