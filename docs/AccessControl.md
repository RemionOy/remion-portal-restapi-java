
# AccessControl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permission** | [**Permission**](Permission.md) |  |  [optional]
**isReadable** | **Boolean** |  |  [optional]
**isWritable** | **Boolean** |  |  [optional]



