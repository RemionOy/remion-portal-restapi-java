
# ObjectId

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **Integer** |  |  [optional]
**machineIdentifier** | **Integer** |  |  [optional]
**processIdentifier** | **Integer** |  |  [optional]
**counter** | **Integer** |  |  [optional]
**time** | **Long** |  |  [optional]
**date** | [**DateTime**](DateTime.md) |  |  [optional]
**timeSecond** | **Integer** |  |  [optional]



