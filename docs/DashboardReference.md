
# DashboardReference

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**dashboard** | [**ReferencedId**](ReferencedId.md) |  |  [optional]



