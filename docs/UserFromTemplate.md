
# UserFromTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**template** | **String** |  | 
**name** | **String** | Email address that will also serve as username |  [optional]
**parent** | **String** |  |  [optional]
**password** | **String** | If this is null, user will be sent an email with activation link, and created user account cannot be used before activation.If this is set, user won&#39;t be sent an email, and user account does not require activation. |  [optional]
**language** | **String** |  |  [optional]



