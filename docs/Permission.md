
# Permission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**permissionType** | [**PermissionTypeEnum**](#PermissionTypeEnum) |  |  [optional]


<a name="PermissionTypeEnum"></a>
## Enum: PermissionTypeEnum
Name | Value
---- | -----
TOKEN | &quot;Token&quot;
REFERENCE | &quot;Reference&quot;
APIKEY | &quot;ApiKey&quot;
RIGHT | &quot;Right&quot;



