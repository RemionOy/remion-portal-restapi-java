
# WidgetSignalListOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allowValueEditing** | **Boolean** |  |  [optional]
**showTrafficLights** | **Boolean** |  |  [optional]
**showTrend** | **Boolean** |  |  [optional]
**options** | **String** |  |  [optional]



