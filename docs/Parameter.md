
# Parameter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**identifier** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**datatype** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**defaultValue** | **String** |  |  [optional]
**latestValue** | **String** |  |  [optional]
**highValue** | **String** |  |  [optional]
**lowValue** | **String** |  |  [optional]
**latestValueTimestamp** | **Long** | Unix time in ms |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**isPublic** | **Boolean** |  |  [optional]
**isReadonly** | **Boolean** |  |  [optional]
**fromTemplate** | **Boolean** |  |  [optional]
**icon** | **String** |  |  [optional]
**metadata** | [**List&lt;Metadata&gt;**](Metadata.md) |  |  [optional]
**actionMapping** | [**List&lt;ActionMapping&gt;**](ActionMapping.md) |  |  [optional]
**location** | [**LocationEmbedded**](LocationEmbedded.md) |  |  [optional]
**isAssetBadge** | **Boolean** |  |  [optional]



