# DeviceApi

All URIs are relative to *https://cloud1.remion.com/regattaportal*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addAssetEvent**](DeviceApi.md#addAssetEvent) | **POST** /v1/device/events | Add event to an asset
[**addData**](DeviceApi.md#addData) | **POST** /v1/device/data | Add new signal sample data. Note that all values for specific timestamp must be in the same row.
[**getMetadataValues**](DeviceApi.md#getMetadataValues) | **GET** /v1/device/metadata | Get parameter values
[**getParameterValues**](DeviceApi.md#getParameterValues) | **GET** /v1/device/parameters | Get parameter values
[**setMetadataValues**](DeviceApi.md#setMetadataValues) | **POST** /v1/device/metadata | Set parameter values
[**setParameterValues**](DeviceApi.md#setParameterValues) | **POST** /v1/device/parameters | Set parameter values
[**updateAssetLocation**](DeviceApi.md#updateAssetLocation) | **POST** /v1/device/location | Update asset location


<a name="addAssetEvent"></a>
# **addAssetEvent**
> addAssetEvent(body, assetId, apikey)

Add event to an asset



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
List<AssetEvent> body = Arrays.asList(new AssetEvent()); // List<AssetEvent> | List of events
String assetId = "assetId_example"; // String | ID of the event's asset
String apikey = "apikey_example"; // String | Asset's apikey
try {
    apiInstance.addAssetEvent(body, assetId, apikey);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#addAssetEvent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**List&lt;AssetEvent&gt;**](AssetEvent.md)| List of events |
 **assetId** | **String**| ID of the event&#39;s asset |
 **apikey** | **String**| Asset&#39;s apikey |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="addData"></a>
# **addData**
> addData(assetId, apikey, body)

Add new signal sample data. Note that all values for specific timestamp must be in the same row.



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
String assetId = "assetId_example"; // String | ID of asset to add samples
String apikey = "apikey_example"; // String | Asset's apikey
DataCollection body = new DataCollection(); // DataCollection | Data values
try {
    apiInstance.addData(assetId, apikey, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#addData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset to add samples | [optional]
 **apikey** | **String**| Asset&#39;s apikey | [optional]
 **body** | [**DataCollection**](DataCollection.md)| Data values | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMetadataValues"></a>
# **getMetadataValues**
> List&lt;ParameterValue&gt; getMetadataValues(assetId, apikey, names)

Get parameter values



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
String assetId = "assetId_example"; // String | ID of asset to add samples
String apikey = "apikey_example"; // String | Asset's apikey
String names = "names_example"; // String | Comma-separated list of metadata names. If null, all metadata values are returned.
try {
    List<ParameterValue> result = apiInstance.getMetadataValues(assetId, apikey, names);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#getMetadataValues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset to add samples | [optional]
 **apikey** | **String**| Asset&#39;s apikey | [optional]
 **names** | **String**| Comma-separated list of metadata names. If null, all metadata values are returned. | [optional]

### Return type

[**List&lt;ParameterValue&gt;**](ParameterValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getParameterValues"></a>
# **getParameterValues**
> List&lt;ParameterValue&gt; getParameterValues(assetId, apikey, identifiers)

Get parameter values



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
String assetId = "assetId_example"; // String | ID of asset
String apikey = "apikey_example"; // String | Asset's apikey
String identifiers = "identifiers_example"; // String | Comma-separated list of parameter identifiers. If null, all parameter values are returned.
try {
    List<ParameterValue> result = apiInstance.getParameterValues(assetId, apikey, identifiers);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#getParameterValues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset | [optional]
 **apikey** | **String**| Asset&#39;s apikey | [optional]
 **identifiers** | **String**| Comma-separated list of parameter identifiers. If null, all parameter values are returned. | [optional]

### Return type

[**List&lt;ParameterValue&gt;**](ParameterValue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="setMetadataValues"></a>
# **setMetadataValues**
> setMetadataValues(assetId, apikey, body)

Set parameter values



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
String assetId = "assetId_example"; // String | ID of asset
String apikey = "apikey_example"; // String | Asset's apikey
List<MetadataValue> body = Arrays.asList(new MetadataValue()); // List<MetadataValue> | Parameter values
try {
    apiInstance.setMetadataValues(assetId, apikey, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#setMetadataValues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset | [optional]
 **apikey** | **String**| Asset&#39;s apikey | [optional]
 **body** | [**List&lt;MetadataValue&gt;**](MetadataValue.md)| Parameter values | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="setParameterValues"></a>
# **setParameterValues**
> setParameterValues(assetId, apikey, body)

Set parameter values



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
String assetId = "assetId_example"; // String | ID of asset
String apikey = "apikey_example"; // String | Asset's apikey
List<ParameterValue> body = Arrays.asList(new ParameterValue()); // List<ParameterValue> | Parameter values
try {
    apiInstance.setParameterValues(assetId, apikey, body);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#setParameterValues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset | [optional]
 **apikey** | **String**| Asset&#39;s apikey | [optional]
 **body** | [**List&lt;ParameterValue&gt;**](ParameterValue.md)| Parameter values | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateAssetLocation"></a>
# **updateAssetLocation**
> updateAssetLocation(assetId, apikey, lat, lon)

Update asset location



### Example
```java
// Import classes:
//import com.remion.portal.restapi.client.ApiException;
//import com.remion.portal.restapi.client.api.DeviceApi;


DeviceApi apiInstance = new DeviceApi();
String assetId = "assetId_example"; // String | ID of asset whose location should be updated
String apikey = "apikey_example"; // String | Asset's apikey
Double lat = 3.4D; // Double | Latitude
Double lon = 3.4D; // Double | Longitude
try {
    apiInstance.updateAssetLocation(assetId, apikey, lat, lon);
} catch (ApiException e) {
    System.err.println("Exception when calling DeviceApi#updateAssetLocation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **assetId** | **String**| ID of asset whose location should be updated |
 **apikey** | **String**| Asset&#39;s apikey |
 **lat** | **Double**| Latitude |
 **lon** | **Double**| Longitude |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

