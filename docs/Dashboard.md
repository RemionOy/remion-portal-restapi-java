
# Dashboard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**accessControl** | [**List&lt;AccessControl&gt;**](AccessControl.md) |  |  [optional]
**name** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**isPublic** | **Boolean** |  |  [optional]
**columns** | **Integer** |  |  [optional]
**columnWidths** | **List&lt;Integer&gt;** | Total width must be 12. E.g. three columns could be split up as 6,3,3 |  [optional]
**widgets** | [**List&lt;Widget&gt;**](Widget.md) |  |  [optional]
**supportedTemplates** | [**List&lt;Asset&gt;**](Asset.md) | Which asset templates this dashboard can view |  [optional]
**isRealtime** | **Boolean** | Whether this is a realtime data dashboard, i.e. is realtime control panel shown |  [optional]
**isTab** | **Boolean** | Whether this dashboard should be its own tab |  [optional]



