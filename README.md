# remion-portal-restapi

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.remion.portal</groupId>
    <artifactId>remion-portal-restapi-java</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.remion.portal:remion-portal-restapi-java:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/remion-portal-restapi-java-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.remion.portal.restapi.client.*;
import com.remion.portal.restapi.client.auth.*;
import com.remion.portal.restapi.client.model.*;
import com.remion.portal.restapi.client.api.DefaultApi;

import java.io.File;
import java.util.*;

public class DefaultApiExample {

    public static void main(String[] args) {
        
        DefaultApi apiInstance = new DefaultApi();
        String assetId = "assetId_example"; // String | ID of asset to add samples
        String apikey = "apikey_example"; // String | Asset's apikey. Deprecated and will be removed in the future.
        DataCollection body = new DataCollection(); // DataCollection | Data values
        String authorization = "authorization_example"; // String | Authorization token, e.g. \"Bearer (token)\". Asset apikey can be used instead for now, but it will be removed in the future.
        try {
            apiInstance.addData(assetId, apikey, body, authorization);
        } catch (ApiException e) {
            System.err.println("Exception when calling DefaultApi#addData");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://cloud1.remion.com/regattaportal*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DefaultApi* | [**addData**](docs/DefaultApi.md#addData) | **POST** /v1/data/{assetId} | Add new signal data. Note that all values for specific timestamp must be in the same row.
*DefaultApi* | [**changeStatus**](docs/DefaultApi.md#changeStatus) | **PUT** /v1/jobs/{jobId} | Start or stop job
*DefaultApi* | [**createAction**](docs/DefaultApi.md#createAction) | **POST** /v1/trigger_actions | Create TriggerAction
*DefaultApi* | [**createAssetFromTemplate**](docs/DefaultApi.md#createAssetFromTemplate) | **POST** /v1/assets | Create asset from template
*DefaultApi* | [**createAssetTemplate**](docs/DefaultApi.md#createAssetTemplate) | **POST** /v1/assets/template | Create asset template based on which new assets can be created
*DefaultApi* | [**createDashboard**](docs/DefaultApi.md#createDashboard) | **POST** /v1/dashboards | Create dashboard
*DefaultApi* | [**createFile**](docs/DefaultApi.md#createFile) | **POST** /v1/files | Create file
*DefaultApi* | [**createImage**](docs/DefaultApi.md#createImage) | **POST** /v1/images | Create image
*DefaultApi* | [**createJob**](docs/DefaultApi.md#createJob) | **POST** /v1/jobs | Create job
*DefaultApi* | [**createPermission**](docs/DefaultApi.md#createPermission) | **POST** /v1/permissions | Create permission
*DefaultApi* | [**createScript**](docs/DefaultApi.md#createScript) | **POST** /v1/scripts | Create script
*DefaultApi* | [**createUser**](docs/DefaultApi.md#createUser) | **POST** /v1/registration | Register new user
*DefaultApi* | [**createUserFromTemplate**](docs/DefaultApi.md#createUserFromTemplate) | **POST** /v1/users | Create user from template
*DefaultApi* | [**createUserTemplate**](docs/DefaultApi.md#createUserTemplate) | **POST** /v1/users/template | Create user template
*DefaultApi* | [**deleteAction**](docs/DefaultApi.md#deleteAction) | **DELETE** /v1/trigger_actions/{triggerActionId} | Delete TriggerAction
*DefaultApi* | [**deleteAsset**](docs/DefaultApi.md#deleteAsset) | **DELETE** /v1/assets/{assetId} | Delete asset. Will also delete all files and events of this asset.
*DefaultApi* | [**deleteDashboard**](docs/DefaultApi.md#deleteDashboard) | **DELETE** /v1/dashboards/{dashboardId} | Delete dashboard
*DefaultApi* | [**deleteData**](docs/DefaultApi.md#deleteData) | **DELETE** /v1/assets/{assetId}/history | Remove asset&#39;s signal data, events, files, and/or location history
*DefaultApi* | [**deleteFile**](docs/DefaultApi.md#deleteFile) | **DELETE** /v1/files/{fileId} | Delete File
*DefaultApi* | [**deleteImage**](docs/DefaultApi.md#deleteImage) | **DELETE** /v1/images/{imageId} | Delete image
*DefaultApi* | [**deleteJob**](docs/DefaultApi.md#deleteJob) | **DELETE** /v1/jobs/{jobId} | Delete job. If job is running, will try to stop it before deleting it.
*DefaultApi* | [**deletePermission**](docs/DefaultApi.md#deletePermission) | **DELETE** /v1/permissions/{permissionId} | Delete permission. Will remove permission from users and access control of objects.
*DefaultApi* | [**deleteScript**](docs/DefaultApi.md#deleteScript) | **DELETE** /v1/scripts/{scriptId} | Delete script
*DefaultApi* | [**deleteUser**](docs/DefaultApi.md#deleteUser) | **DELETE** /v1/users/{userId} | Delete user
*DefaultApi* | [**exportData**](docs/DefaultApi.md#exportData) | **GET** /v1/data/{assetId}/export | Export signal data to csv file
*DefaultApi* | [**exportEvents**](docs/DefaultApi.md#exportEvents) | **GET** /v1/events/export | Export list of asset events. Produces CSV file.
*DefaultApi* | [**getAction**](docs/DefaultApi.md#getAction) | **GET** /v1/trigger_actions/{triggerActionId} | Get specific TriggerAction
*DefaultApi* | [**getActionCount**](docs/DefaultApi.md#getActionCount) | **GET** /v1/trigger_actions/count | Get TriggerAction count
*DefaultApi* | [**getActions**](docs/DefaultApi.md#getActions) | **GET** /v1/trigger_actions | Get list of TriggerActions user has permissions to see
*DefaultApi* | [**getAsset**](docs/DefaultApi.md#getAsset) | **GET** /v1/assets/{assetId} | Get specific asset
*DefaultApi* | [**getAssetHierarchy**](docs/DefaultApi.md#getAssetHierarchy) | **GET** /v1/assets/hierarchy | Get asset hierarchy
*DefaultApi* | [**getAssets**](docs/DefaultApi.md#getAssets) | **GET** /v1/assets | Get list of assets user has permissions to see
*DefaultApi* | [**getDashboard**](docs/DefaultApi.md#getDashboard) | **GET** /v1/dashboards/{dashboardId} | Get specific dashboard
*DefaultApi* | [**getDashboardCount**](docs/DefaultApi.md#getDashboardCount) | **GET** /v1/dashboards/count | Get dashboard count
*DefaultApi* | [**getDashboards**](docs/DefaultApi.md#getDashboards) | **GET** /v1/dashboards | Get list of dashboards
*DefaultApi* | [**getData**](docs/DefaultApi.md#getData) | **GET** /v1/data/{assetId} | Get existing signal data
*DefaultApi* | [**getEventCount**](docs/DefaultApi.md#getEventCount) | **GET** /v1/events/count | Get asset event count
*DefaultApi* | [**getEvents**](docs/DefaultApi.md#getEvents) | **GET** /v1/events | Get list of asset events
*DefaultApi* | [**getFile**](docs/DefaultApi.md#getFile) | **GET** /v1/files/{fileId} | Get specific file
*DefaultApi* | [**getFileCount**](docs/DefaultApi.md#getFileCount) | **GET** /v1/files/count | Get file count
*DefaultApi* | [**getFiles**](docs/DefaultApi.md#getFiles) | **GET** /v1/files | Get list of files user has permissions to see
*DefaultApi* | [**getImage**](docs/DefaultApi.md#getImage) | **GET** /v1/images/{imageId} | Get specific image
*DefaultApi* | [**getImageCount**](docs/DefaultApi.md#getImageCount) | **GET** /v1/images/count | Get image count
*DefaultApi* | [**getImages**](docs/DefaultApi.md#getImages) | **GET** /v1/images | Get list of images  or find an image by name
*DefaultApi* | [**getJob**](docs/DefaultApi.md#getJob) | **GET** /v1/jobs/{jobId} | Get specific job
*DefaultApi* | [**getJobCount**](docs/DefaultApi.md#getJobCount) | **GET** /v1/jobs/count | Get job count
*DefaultApi* | [**getJobs**](docs/DefaultApi.md#getJobs) | **GET** /v1/jobs | Get list of jobs user has permissions to see
*DefaultApi* | [**getLocationCount**](docs/DefaultApi.md#getLocationCount) | **GET** /v1/locations/count | Get location count
*DefaultApi* | [**getLocations**](docs/DefaultApi.md#getLocations) | **GET** /v1/locations | Get list of locations
*DefaultApi* | [**getPermission**](docs/DefaultApi.md#getPermission) | **GET** /v1/permissions/{permissionId} | Get specific permission
*DefaultApi* | [**getPermissions**](docs/DefaultApi.md#getPermissions) | **GET** /v1/permissions | Get list of permissions user has permissions to see
*DefaultApi* | [**getScript**](docs/DefaultApi.md#getScript) | **GET** /v1/scripts/{scriptId} | Get specific script
*DefaultApi* | [**getScriptCount**](docs/DefaultApi.md#getScriptCount) | **GET** /v1/scripts/count | Get script count
*DefaultApi* | [**getScripts**](docs/DefaultApi.md#getScripts) | **GET** /v1/scripts | Get list of script user has permission to see
*DefaultApi* | [**getServerInfo**](docs/DefaultApi.md#getServerInfo) | **GET** /v1/serverinfo | Get basic information of the server, e.g. version
*DefaultApi* | [**getStatus**](docs/DefaultApi.md#getStatus) | **GET** /v1/status | Get user specific status
*DefaultApi* | [**getUISettings**](docs/DefaultApi.md#getUISettings) | **GET** /v1/ui_settings | Get basic UI settings
*DefaultApi* | [**getUser**](docs/DefaultApi.md#getUser) | **GET** /v1/users/{userId} | Get user
*DefaultApi* | [**getUserCount**](docs/DefaultApi.md#getUserCount) | **GET** /v1/users/count | Get user count
*DefaultApi* | [**getUsers**](docs/DefaultApi.md#getUsers) | **GET** /v1/users | Get list of users user has permissions to see
*DefaultApi* | [**passwordResetRequest**](docs/DefaultApi.md#passwordResetRequest) | **POST** /v1/pwreset | Ask for password reset
*DefaultApi* | [**refreshToken**](docs/DefaultApi.md#refreshToken) | **POST** /v1/session | Get session token
*DefaultApi* | [**resetPassword**](docs/DefaultApi.md#resetPassword) | **PUT** /v1/pwreset | Reset password
*DefaultApi* | [**updateAction**](docs/DefaultApi.md#updateAction) | **PUT** /v1/trigger_actions/{triggerActionId} | Update action
*DefaultApi* | [**updateAsset**](docs/DefaultApi.md#updateAsset) | **PUT** /v1/assets/{assetId} | Update asset
*DefaultApi* | [**updateAssetTemplate**](docs/DefaultApi.md#updateAssetTemplate) | **POST** /v1/assets/{assetId}/template | Update asset template: migrates asset to different template version
*DefaultApi* | [**updateDashboard**](docs/DefaultApi.md#updateDashboard) | **PUT** /v1/dashboards/{dashboardId} | Update dashboard
*DefaultApi* | [**updateFile**](docs/DefaultApi.md#updateFile) | **PUT** /v1/files/{fileId} | Update file
*DefaultApi* | [**updateImage**](docs/DefaultApi.md#updateImage) | **PUT** /v1/images/{imageId} | Update image
*DefaultApi* | [**updatePermission**](docs/DefaultApi.md#updatePermission) | **PUT** /v1/permissions/{permissionId} | Update permission
*DefaultApi* | [**updateScript**](docs/DefaultApi.md#updateScript) | **PUT** /v1/scripts/{scriptId} | Update image
*DefaultApi* | [**updateUser**](docs/DefaultApi.md#updateUser) | **PUT** /v1/users/{userId} | Update user
*DefaultApi* | [**updateUserTemplate**](docs/DefaultApi.md#updateUserTemplate) | **POST** /v1/users/{userId}/template | Update user template: migrates user to different template version
*DeviceApi* | [**addAssetEvent**](docs/DeviceApi.md#addAssetEvent) | **POST** /v1/device/events | Add event to an asset
*DeviceApi* | [**addData**](docs/DeviceApi.md#addData) | **POST** /v1/device/data | Add new signal sample data. Note that all values for specific timestamp must be in the same row.
*DeviceApi* | [**getMetadataValues**](docs/DeviceApi.md#getMetadataValues) | **GET** /v1/device/metadata | Get parameter values
*DeviceApi* | [**getParameterValues**](docs/DeviceApi.md#getParameterValues) | **GET** /v1/device/parameters | Get parameter values
*DeviceApi* | [**setMetadataValues**](docs/DeviceApi.md#setMetadataValues) | **POST** /v1/device/metadata | Set parameter values
*DeviceApi* | [**setParameterValues**](docs/DeviceApi.md#setParameterValues) | **POST** /v1/device/parameters | Set parameter values
*DeviceApi* | [**updateAssetLocation**](docs/DeviceApi.md#updateAssetLocation) | **POST** /v1/device/location | Update asset location


## Documentation for Models

 - [AccessControl](docs/AccessControl.md)
 - [Action](docs/Action.md)
 - [ActionMapping](docs/ActionMapping.md)
 - [Asset](docs/Asset.md)
 - [AssetEvent](docs/AssetEvent.md)
 - [AssetFile](docs/AssetFile.md)
 - [AssetFromTemplate](docs/AssetFromTemplate.md)
 - [AssetHistoryRemovalParams](docs/AssetHistoryRemovalParams.md)
 - [Assets](docs/Assets.md)
 - [BaseRegisteringUser](docs/BaseRegisteringUser.md)
 - [Count](docs/Count.md)
 - [Credentials](docs/Credentials.md)
 - [Dashboard](docs/Dashboard.md)
 - [DashboardReference](docs/DashboardReference.md)
 - [DataCollection](docs/DataCollection.md)
 - [DataValueRow](docs/DataValueRow.md)
 - [ErrorResponse](docs/ErrorResponse.md)
 - [Event](docs/Event.md)
 - [Image](docs/Image.md)
 - [Job](docs/Job.md)
 - [LocationEmbedded](docs/LocationEmbedded.md)
 - [LocationEntity](docs/LocationEntity.md)
 - [Metadata](docs/Metadata.md)
 - [MetadataValue](docs/MetadataValue.md)
 - [MigrationResult](docs/MigrationResult.md)
 - [ObjectId](docs/ObjectId.md)
 - [Parameter](docs/Parameter.md)
 - [ParameterValue](docs/ParameterValue.md)
 - [PasswordResetRequest](docs/PasswordResetRequest.md)
 - [Permission](docs/Permission.md)
 - [ReferencedId](docs/ReferencedId.md)
 - [ResourceCreatedResponse](docs/ResourceCreatedResponse.md)
 - [Script](docs/Script.md)
 - [ServerInfo](docs/ServerInfo.md)
 - [SessionResponse](docs/SessionResponse.md)
 - [Signal](docs/Signal.md)
 - [Status](docs/Status.md)
 - [Trigger](docs/Trigger.md)
 - [TriggerAction](docs/TriggerAction.md)
 - [UISettings](docs/UISettings.md)
 - [User](docs/User.md)
 - [UserDashboardConfig](docs/UserDashboardConfig.md)
 - [UserFromTemplate](docs/UserFromTemplate.md)
 - [Widget](docs/Widget.md)
 - [WidgetAlertOptions](docs/WidgetAlertOptions.md)
 - [WidgetBarChartOptions](docs/WidgetBarChartOptions.md)
 - [WidgetCameraOptions](docs/WidgetCameraOptions.md)
 - [WidgetCanvasItem](docs/WidgetCanvasItem.md)
 - [WidgetCanvasOptions](docs/WidgetCanvasOptions.md)
 - [WidgetCustomOptions](docs/WidgetCustomOptions.md)
 - [WidgetImageOptions](docs/WidgetImageOptions.md)
 - [WidgetLabelOptions](docs/WidgetLabelOptions.md)
 - [WidgetLineChartOptions](docs/WidgetLineChartOptions.md)
 - [WidgetPieChartOptions](docs/WidgetPieChartOptions.md)
 - [WidgetRadialChartOptions](docs/WidgetRadialChartOptions.md)
 - [WidgetSignalListOptions](docs/WidgetSignalListOptions.md)
 - [WidgetTextOptions](docs/WidgetTextOptions.md)


## Documentation for Authorization

All endpoints do not require authorization.
Authentication schemes defined for the API:

## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issue.

## Author



