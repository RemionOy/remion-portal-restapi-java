/**
 * Portal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.remion.portal.restapi.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import com.remion.portal.restapi.client.model.AccessControl;
import com.remion.portal.restapi.client.model.Asset;
import com.remion.portal.restapi.client.model.Widget;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;


/**
 * Dashboard
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-08-29T13:25:14.657+03:00")
public class Dashboard   {
  @SerializedName("id")
  private String id = null;

  @SerializedName("accessControl")
  private List<AccessControl> accessControl = new ArrayList<AccessControl>();

  @SerializedName("name")
  private String name = null;

  @SerializedName("type")
  private String type = null;

  @SerializedName("description")
  private String description = null;

  @SerializedName("isPublic")
  private Boolean isPublic = false;

  @SerializedName("columns")
  private Integer columns = null;

  @SerializedName("columnWidths")
  private List<Integer> columnWidths = new ArrayList<Integer>();

  @SerializedName("widgets")
  private List<Widget> widgets = new ArrayList<Widget>();

  @SerializedName("supportedTemplates")
  private List<Asset> supportedTemplates = new ArrayList<Asset>();

  @SerializedName("isRealtime")
  private Boolean isRealtime = false;

  @SerializedName("isTab")
  private Boolean isTab = false;

  public Dashboard id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Dashboard accessControl(List<AccessControl> accessControl) {
    this.accessControl = accessControl;
    return this;
  }

  public Dashboard addAccessControlItem(AccessControl accessControlItem) {
    this.accessControl.add(accessControlItem);
    return this;
  }

   /**
   * Get accessControl
   * @return accessControl
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<AccessControl> getAccessControl() {
    return accessControl;
  }

  public void setAccessControl(List<AccessControl> accessControl) {
    this.accessControl = accessControl;
  }

  public Dashboard name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Dashboard type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Dashboard description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Dashboard isPublic(Boolean isPublic) {
    this.isPublic = isPublic;
    return this;
  }

   /**
   * Get isPublic
   * @return isPublic
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getIsPublic() {
    return isPublic;
  }

  public void setIsPublic(Boolean isPublic) {
    this.isPublic = isPublic;
  }

  public Dashboard columns(Integer columns) {
    this.columns = columns;
    return this;
  }

   /**
   * Get columns
   * @return columns
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getColumns() {
    return columns;
  }

  public void setColumns(Integer columns) {
    this.columns = columns;
  }

  public Dashboard columnWidths(List<Integer> columnWidths) {
    this.columnWidths = columnWidths;
    return this;
  }

  public Dashboard addColumnWidthsItem(Integer columnWidthsItem) {
    this.columnWidths.add(columnWidthsItem);
    return this;
  }

   /**
   * Total width must be 12. E.g. three columns could be split up as 6,3,3
   * @return columnWidths
  **/
  @ApiModelProperty(example = "null", value = "Total width must be 12. E.g. three columns could be split up as 6,3,3")
  public List<Integer> getColumnWidths() {
    return columnWidths;
  }

  public void setColumnWidths(List<Integer> columnWidths) {
    this.columnWidths = columnWidths;
  }

  public Dashboard widgets(List<Widget> widgets) {
    this.widgets = widgets;
    return this;
  }

  public Dashboard addWidgetsItem(Widget widgetsItem) {
    this.widgets.add(widgetsItem);
    return this;
  }

   /**
   * Get widgets
   * @return widgets
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<Widget> getWidgets() {
    return widgets;
  }

  public void setWidgets(List<Widget> widgets) {
    this.widgets = widgets;
  }

  public Dashboard supportedTemplates(List<Asset> supportedTemplates) {
    this.supportedTemplates = supportedTemplates;
    return this;
  }

  public Dashboard addSupportedTemplatesItem(Asset supportedTemplatesItem) {
    this.supportedTemplates.add(supportedTemplatesItem);
    return this;
  }

   /**
   * Which asset templates this dashboard can view
   * @return supportedTemplates
  **/
  @ApiModelProperty(example = "null", value = "Which asset templates this dashboard can view")
  public List<Asset> getSupportedTemplates() {
    return supportedTemplates;
  }

  public void setSupportedTemplates(List<Asset> supportedTemplates) {
    this.supportedTemplates = supportedTemplates;
  }

  public Dashboard isRealtime(Boolean isRealtime) {
    this.isRealtime = isRealtime;
    return this;
  }

   /**
   * Whether this is a realtime data dashboard, i.e. is realtime control panel shown
   * @return isRealtime
  **/
  @ApiModelProperty(example = "null", value = "Whether this is a realtime data dashboard, i.e. is realtime control panel shown")
  public Boolean getIsRealtime() {
    return isRealtime;
  }

  public void setIsRealtime(Boolean isRealtime) {
    this.isRealtime = isRealtime;
  }

  public Dashboard isTab(Boolean isTab) {
    this.isTab = isTab;
    return this;
  }

   /**
   * Whether this dashboard should be its own tab
   * @return isTab
  **/
  @ApiModelProperty(example = "null", value = "Whether this dashboard should be its own tab")
  public Boolean getIsTab() {
    return isTab;
  }

  public void setIsTab(Boolean isTab) {
    this.isTab = isTab;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dashboard dashboard = (Dashboard) o;
    return Objects.equals(this.id, dashboard.id) &&
        Objects.equals(this.accessControl, dashboard.accessControl) &&
        Objects.equals(this.name, dashboard.name) &&
        Objects.equals(this.type, dashboard.type) &&
        Objects.equals(this.description, dashboard.description) &&
        Objects.equals(this.isPublic, dashboard.isPublic) &&
        Objects.equals(this.columns, dashboard.columns) &&
        Objects.equals(this.columnWidths, dashboard.columnWidths) &&
        Objects.equals(this.widgets, dashboard.widgets) &&
        Objects.equals(this.supportedTemplates, dashboard.supportedTemplates) &&
        Objects.equals(this.isRealtime, dashboard.isRealtime) &&
        Objects.equals(this.isTab, dashboard.isTab);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, accessControl, name, type, description, isPublic, columns, columnWidths, widgets, supportedTemplates, isRealtime, isTab);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Dashboard {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    accessControl: ").append(toIndentedString(accessControl)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    isPublic: ").append(toIndentedString(isPublic)).append("\n");
    sb.append("    columns: ").append(toIndentedString(columns)).append("\n");
    sb.append("    columnWidths: ").append(toIndentedString(columnWidths)).append("\n");
    sb.append("    widgets: ").append(toIndentedString(widgets)).append("\n");
    sb.append("    supportedTemplates: ").append(toIndentedString(supportedTemplates)).append("\n");
    sb.append("    isRealtime: ").append(toIndentedString(isRealtime)).append("\n");
    sb.append("    isTab: ").append(toIndentedString(isTab)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

