/**
 * Portal API
 * Regatta portal backend API
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.remion.portal.restapi.client.model;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;


/**
 * DataValueRow
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.JavaClientCodegen", date = "2016-08-29T13:25:14.657+03:00")
public class DataValueRow   {
  @SerializedName("timestamp")
  private Long timestamp = null;

  @SerializedName("values")
  private List<Object> values = new ArrayList<Object>();

  public DataValueRow timestamp(Long timestamp) {
    this.timestamp = timestamp;
    return this;
  }

   /**
   * Unix time in ms
   * @return timestamp
  **/
  @ApiModelProperty(example = "null", value = "Unix time in ms")
  public Long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Long timestamp) {
    this.timestamp = timestamp;
  }

  public DataValueRow values(List<Object> values) {
    this.values = values;
    return this;
  }

  public DataValueRow addValuesItem(Object valuesItem) {
    this.values.add(valuesItem);
    return this;
  }

   /**
   * Supported values in array are string-number maps and numbers. If there aremultiple rows, each map of given signal must have same keys.
   * @return values
  **/
  @ApiModelProperty(example = "null", value = "Supported values in array are string-number maps and numbers. If there aremultiple rows, each map of given signal must have same keys.")
  public List<Object> getValues() {
    return values;
  }

  public void setValues(List<Object> values) {
    this.values = values;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DataValueRow dataValueRow = (DataValueRow) o;
    return Objects.equals(this.timestamp, dataValueRow.timestamp) &&
        Objects.equals(this.values, dataValueRow.values);
  }

  @Override
  public int hashCode() {
    return Objects.hash(timestamp, values);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DataValueRow {\n");
    
    sb.append("    timestamp: ").append(toIndentedString(timestamp)).append("\n");
    sb.append("    values: ").append(toIndentedString(values)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

